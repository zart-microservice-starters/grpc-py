import asyncio
import logging

import sentry_sdk
from grpclib.server import Stream, Server
from grpclib.utils import graceful_exit

from src.clients import MessageClient
from src.proto.hellopy_grpc import GreeterBase
from src.proto.hellopy_pb2 import HelloRequest, HelloReply
from src.utils import load_config, setup_logger
from src.domain import Messaging


class Greeter(GreeterBase):  # type: ignore
    async def SayHello(self, stream: Stream[HelloRequest, HelloReply]) -> None:
        messaging = Messaging()
        await messaging.init(message_client=MessageClient())
        request = await stream.recv_message()
        message = await messaging.create(name=request.name)  # type: ignore
        await stream.send_message(HelloReply(message=message))


async def main() -> None:
    config = await load_config()
    sentry_sdk.init(config.sentry_url)
    setup_logger(config.log_level)
    server = Server([Greeter()])
    with graceful_exit([server]):
        await server.start(str(config.host), config.port)
        logging.info(f"Running with Config: {config}")
        await server.wait_closed()


if __name__ == "__main__":
    asyncio.run(main())

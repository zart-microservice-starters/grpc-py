from dataclasses import dataclass
from abc import ABC, abstractmethod


@dataclass
class Message:
    name: str


class ClientBase(ABC):
    @abstractmethod
    async def init(self) -> None:
        pass

    @abstractmethod
    async def create(self, message: Message) -> None:
        pass

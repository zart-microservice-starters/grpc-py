from typing import TypeVar

from src.models import ClientBase, Message


C_contra = TypeVar(  # pylint: disable=invalid-name
    "C_contra", bound=ClientBase, contravariant=True
)


class Messaging:
    async def init(self, message_client: C_contra) -> None:
        self.message_client = message_client
        await self.message_client.init()

    async def create(self, name: str) -> str:
        message = f"Hello {name} from hellopy"
        await self.message_client.create(Message(name=name))
        return message

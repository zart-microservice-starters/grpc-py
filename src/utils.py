import logging
import os
from ipaddress import IPv4Address
from typing import Literal, Optional

import hvac
from pydantic import BaseModel, HttpUrl, Field
import yaml
import aiofiles

DOMAIN_REGEX = r"^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$"

ENV = os.environ.get("ENV", "local")


class Config(BaseModel):
    log_level: Literal["ERROR", "WARN", "INFO", "DEBUG"] = "DEBUG"
    sentry_url: Optional[HttpUrl] = None
    port: int
    host: IPv4Address
    e2e: bool
    domain: str = Field(..., regex=DOMAIN_REGEX)


class ConfigMap(BaseModel):
    local: Config
    development: Config
    beta: Config
    production: Config


async def load_config() -> Config:
    async with aiofiles.open("cfg.yml", mode="rb") as file:
        contents = await file.read()
        config_map = ConfigMap(**yaml.safe_load(contents))
        match ENV:
            case "local":
                return config_map.local
            case "development":
                return config_map.development
            case "beta":
                return config_map.beta
            case _:
                return config_map.production


def setup_logger(level: str) -> None:
    # filters out noisy sub loggers. varies by project
    logging.getLogger("hpack").setLevel(logging.CRITICAL)

    logging.addLevelName(
        logging.ERROR,
        "\033[0;31m%s\033[1;0m"  # pylint: disable=consider-using-f-string
        % logging.getLevelName(logging.ERROR),
    )
    logging.addLevelName(
        logging.WARNING,
        "\033[0;33m%s\033[1;0m"  # pylint: disable=consider-using-f-string
        % logging.getLevelName(logging.WARNING),
    )
    logging.addLevelName(
        logging.INFO,
        "\033[0;32m%s\033[1;0m"  # pylint: disable=consider-using-f-string
        % logging.getLevelName(logging.INFO),
    )
    logging.addLevelName(
        logging.DEBUG,
        "\033[0;34m%s\033[1;0m"  # pylint: disable=consider-using-f-string
        % logging.getLevelName(logging.DEBUG),
    )
    logging.basicConfig(
        encoding="utf-8",
        format="%(levelname)s: %(message)s",
        level=level,
    )


async def get_secret(secret_name: str) -> str:
    config = await load_config()
    client = hvac.Client(
        url=f"https://vault.{config.domain}", token=os.getenv("VAULT_TOKEN")
    )
    assert client.is_authenticated()
    response = client.secrets.kv.v2.read_secret_version(path=f"{ENV}/{secret_name}")
    return str(response["data"]["data"][secret_name])

import pytest

from src.domain import Messaging

from src.models import ClientBase, Message


class MockMessageClient(ClientBase):
    async def init(self) -> None:
        pass

    async def create(self, message: Message) -> None:
        pass


@pytest.mark.asyncio
async def test_creates_message() -> None:
    messaging = Messaging()
    await messaging.init(message_client=MockMessageClient())
    actual = await messaging.create(name="test")
    assert actual == "Hello test from hellopy"

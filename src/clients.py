from sqlmodel import SQLModel
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession

from src.entities import MessageEntity
from src.models import Message, ClientBase
from src.utils import get_secret, load_config, ENV


class MessageClient(ClientBase):
    async def init(self) -> None:
        db_password = await get_secret("py_db_password")
        config = await load_config()
        db_url = f"postgresql+asyncpg://hello:{db_password}@{config.domain}:5432/hellopy_{ENV}"
        self.engine = create_async_engine(db_url, future=True)
        async with self.engine.begin() as conn:
            await conn.run_sync(SQLModel.metadata.create_all)

    async def create(self, message: Message) -> None:
        message_entity = MessageEntity(name=message.name)
        async with AsyncSession(self.engine) as session:
            session.add(message_entity)
            await session.commit()

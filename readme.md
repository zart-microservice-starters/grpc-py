# grpc-py 🐍

![](https://img.shields.io/gitlab/pipeline-status/zart-microservice-starters/grpc-py?branch=development&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/zart-microservice-starters/grpc-py/development?logo=pytest&style=for-the-badge)
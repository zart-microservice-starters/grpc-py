provider "vault" {
  address          = "https://vault.arketyped.net"
  skip_child_token = true
}

variable "ENV" {
  type        = string
  description = "The environment the services are running in. Can only be local, development, bera, or production."
  default     = "local"
}

resource "random_password" "password" {
  length  = 16
  special = false
}

resource "vault_generic_secret" "secret" {
  path = "secret/${var.ENV}/py_db_password"
  data_json = jsonencode(
    {
      "py_db_password" = random_password.password.result
    }
  )
}
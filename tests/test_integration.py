from typing import Tuple

import pytest
from grpclib.client import Channel
from grpclib.utils import graceful_exit
from grpclib.server import Server

from src.proto.hellopy_grpc import GreeterStub
from src.proto.hellopy_pb2 import HelloRequest
from src.utils import load_config
from src.grpc import Greeter


async def init() -> Tuple[GreeterStub, Server]:
    config = await load_config()
    server = Server([Greeter()])
    with graceful_exit([server]):
        await server.start(str(config.host), config.port)
    async with Channel(str(config.host), config.port) as channel:
        return GreeterStub(channel), server


@pytest.mark.asyncio
async def test_create_message() -> None:
    (client, server) = await init()
    reply = await client.SayHello(HelloRequest(name="test"))
    assert reply.message == "Hello test from hellopy"
    server.close()
